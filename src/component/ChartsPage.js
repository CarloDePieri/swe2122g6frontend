
import { Pie } from "react-chartjs-2";
import { MDBContainer } from "mdbreact";
import React,{useEffect, useState} from 'react'
import * as call from '../utils/call.js'

function ChartsPage(props) {
  const [settings, setSettings]= useState({
    dataPie: {
      labels: ["Negative", "Neutral", "Positive"],
      datasets: [
        {
          data: [],
          backgroundColor: [
            "#f7464a",
            "#46bfbd",
            "#fdb45c",
          ],
          hoverBackgroundColor: [
            "#ff5a5e",
            "#5ad3d1",
            "#ffc870",
          ]
        }
      ]
    }
  }
  )

const [metrics, setMetrics] = useState({
  negative_tweets_count: 0,
  neutral_tweets_count: 0,
  positive_tweets_count: 0,
  total_tweets_count: 0
})
const [loading,setLoading] = useState(true);

useEffect(()=>{
  async function getSentiment(){
    let data = {
      to_score: props.results.map( x => {
          let obj = {
            text: '',
            lang: ''
          }
          obj.text = x.text;
          obj.lang = x.lang;
          return obj
        })
    }

    let arr = [];
    let newRes={
      negative_tweets_count: 0,
      neutral_tweets_count: 0,
      positive_tweets_count: 0,
      total_tweets_count: 0
    }

    for (var i = 0; i < data.to_score.length; i++) {
      arr.push(data.to_score[i]);
      if((i%10 == 0 ) || (i==data.to_score.length-1))
      {
        const res = await call.getSentiment({to_score: arr})
        newRes = {
          negative_tweets_count: (res.metrics.negative_tweets_count+newRes.negative_tweets_count),
          neutral_tweets_count: res.metrics.neutral_tweets_count+newRes.neutral_tweets_count,
          positive_tweets_count: res.metrics.positive_tweets_count+newRes.positive_tweets_count,
          total_tweets_count: res.metrics.total_tweets_count+newRes.total_tweets_count
        }
        const arr2 =[newRes.negative_tweets_count, newRes.neutral_tweets_count,newRes.positive_tweets_count];
        arr = []
        setSettings({
                        ...settings,
                        dataPie: {
                          ...settings.dataPie,
                          datasets: [{
                            ...settings.dataPie.datasets[0],
                            data: arr2
                          }]
                        },
                      })

      }
    }

    setLoading(false)
  }
getSentiment();


},[props.results])

    return (
      <MDBContainer>
        <h3 className="mt-5">Sentiment analysis chart.</h3>
        <Pie data={settings.dataPie} options={{ responsive: true }} />
      </MDBContainer>
    );
  }

export default ChartsPage;
