import "d3-transition";
import { select } from "d3-selection";
import ReactWordcloud from "react-wordcloud";
import React,{ useState, useEffect } from "react";
import "tippy.js/dist/tippy.css";
import "tippy.js/animations/scale.css";
import * as utils from '../utils/scripts'

function getCallback(callback) {
  return function (word, event) {
  const isActive = callback !== "onWordMouseOut";
  const element = event.target;
  const text = select(element);
  text
    .on("click", () => {
      if(isActive) {
        window.open(`https://duckduckgo.com/?q=${word.text}`, "_blank");
      }
    })
    .transition()
    .attr("background", "white")
    .attr("font-size", isActive ? "300%" : "100%")
    .attr("text-decoration", isActive ? "underline" : "none");
};
}

const callbacks = {
  getWordTooltip: (word) =>
    `The word "${word.text}" appears ${word.value} times.`,
  onWordClick: getCallback("onWordClick"),
  onWordMouseOut: getCallback("onWordMouseOut"),
  onWordMouseOver: getCallback("onWordMouseOver")
};

const options = {
  colors: ["#1f77b4", "#ff7f0e", "#2ca02c", "#d62728", "#9467bd", "#8c564b"],
  enableTooltip: true,
  deterministic: false,
  fontSizes: [5, 60],
  fontStyle: "normal",
  fontWeight: "normal",
  padding: 1,
  rotations: 4,
  rotationAngles: [0, 90],
  scale: "sqrt",
  spiral: "archimedean",
  transitionDuration: 1400
};


function WordCloud({tweets,eventKey}) {
  const [timedOption, setTimedOptions] = useState(options);
  let words = utils.getWordCount(tweets)
  useEffect(() => {
    setTimedOptions(options);
    words = utils.getWordCount(tweets)
  }, [options]);

  return (
    <div>
      <div style={{ height: 400, width: 400 }}>
        {eventKey=="wordcloud"?<ReactWordcloud
          options={timedOption}
          callbacks={callbacks}
          words={words}
        />:<p></p>}
      </div>
    </div>
  )
}
export default WordCloud;
