import React from "react"

function Loader()
{
  return(
    <div style={{float: "left"}} className="d-flex justify-content-center">
      <div className="loader"/>
    </div>
  )
}

export default Loader;
