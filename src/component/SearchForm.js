import TabsContainer from "./TabsContainer"
import ContainerTweet from "./ContainerTweet"
import {Container} from "@chakra-ui/react"
import {Col,Row,Collapse} from "react-bootstrap"
import Form from 'react-bootstrap/Form'
import Button from 'react-bootstrap/Button'
import Modal from 'react-bootstrap/Modal'
import React, {useState} from 'react'
import * as call from '../utils/call.js'
import MapFilter from './MapFilter'

function SearchForm (props){
  const onSubmitFn = props.onSubmit;
  const [show, setShow] = useState(false);
  const [toggleActive, setToggleActive] = useState(false);
  const [inputText, setInputText] = useState(props.inputText);
  const [eventsSource, setEventsSource] = useState(null);
  const [checked,setChecked] = useState(false);
  const [user, setUser] = useState("");
  const [place, setPlace] = useState("");
  const [hasGeo, setHasGeo] = useState(false)
  const [loading, setLoading] = useState(false);
  const [results, setResults] = useState();
  const [distribution, setDistribution] = useState({"distribution" : []});
  const [tweetNumberLimit, setTweetNumberLimit] = useState(10);
  const [disabled,setDisabled] = useState(true);
  const [open, setOpen] = useState(false);
  const [bbox, setBbox] = useState();

  const getTweet= async (e) =>
  {
    e.preventDefault()
    setDisabled(false)
    //let searchText = (e.target[0].value).replace('#','%23')
    //setInputText(searchText);
    try {
      const result = await call.getTweets(inputText, user, place, tweetNumberLimit);
      setResults(result.results)
      setDistribution(result.distribution_results)
      setLoading(true);
    }
    catch (err) {
      console.log(err)
    }
  }

  function handleModalHide(){
    setShow(false);
    console.log(bbox);
    if(!open)
    {
      setBbox(null);
    }
  }

  function setQuery(e){
    e.preventDefault();
    console.log((e.target.value).replace('#','%23'))
    setInputText((e.target.value).replace('#','%23'))
  }


  function setFiltrerUser(e){
    e.preventDefault();
    console.log(e.target.value)
    setUser(e.target.value)
  }

  function setFiltrerPlace(e){
    e.preventDefault();
    console.log(e.target.value)
    setPlace(e.target.value)
  }

  function closeStream() {
    if (eventsSource!=null) {
      eventsSource.close()
      setEventsSource(null)
    }
  }

  function toggleStream(checked) {
    setChecked(!checked);
    if((inputText!="")&&(!checked))
    {

      if (eventsSource === null) {
        const es = new EventSource(`http://localhost:8000/api/stream?keywords=${inputText}`);
        setEventsSource(es)
        let data = [...results];
        es.onmessage = event => {
          const parsedData = JSON.parse(event.data);
          data = [parsedData, ...data]
          console.log(data)
          setResults(data)
        };
        es.onerror = error => {
          closeStream()
          setChecked(false)
        }
      }
      else {
        closeStream()
      }
    }
    else {
      closeStream()
    }
  }




return(
  <Container className="sticky-top pb-2" bg="#f3f9ff" zIndex="1019">
    <Form onSubmit={getTweet} style={{paddingTop: "5rem"}}>
      <Form.Group controlId="formGridEmail">
        <Form.Label className="h3 mb-4" style={{font:"Roboto"}}>Cerca i tweet che ti interessano</Form.Label>
        <Container className="mb-4 d-flex">
          <Container className="flex-grow-1">
            <div>
              <Form.Control onChange={setQuery} type="text" placeholder="eg: #newyork" value={inputText}/>
            </div>
          </Container>
          <Container className="px-2">
            <Button variant="primary" type="submit" className="pl-2">
              Cerca
            </Button>
          </Container>
          <Button variant="primary" onClick={() => setShow(!show)}>
            Filtri
          </Button>
          &nbsp;
          <Button disabled={disabled} variant={checked?"success":"danger"} onClick={() => toggleStream(checked)}>
              Stream
            </Button>
          <Modal
            show={show}
            fullscreen={true}
            onHide={handleModalHide}
            size="lg"
            aria-labelledby="contained-modal-title-vcenter"
            centered
          >
            <Modal.Header closeButton>
              <Modal.Title>Filtri ricerca:</Modal.Title>
            </Modal.Header>
            <Modal.Body>
                <Form style={{paddingTop: "1rem"}}>
                  <Form.Group controlId="formGridEmail">
                    <Container className="mb-4 d-flex">
                      <Container className="flex-grow-1">
                        <Row>
                          <Col>
                            <Form.Label className="h4 mb-4">Query: </Form.Label>
                            <div>
                              <input onChange={setQuery} type="text" placeholder="eg: #newyork" value={inputText}/>
                            </div>
                          </Col>
                          <Col>
                            <Form.Label className="h4 mb-4">Nome Utente: </Form.Label>
                            <div>
                              <input onChange={setFiltrerUser} type="text" placeholder="eg: username" value={user}/>
                            </div>
                          </Col>
                        </Row>
                        <hr></hr>
                        <Row>
                          <Col>
                            <Form.Label className="h4 mb-4">Luogo: </Form.Label>
                            <div>
                              <input onChange={setFiltrerPlace} type="text" placeholder="eg: place" value={place} />
                            </div>
                          </Col>
                          <Col>
                            <Form.Label className="h4 mb-4"> Geolocalizzato: </Form.Label>
                            <div>
                              <input onChange={setHasGeo} type="text" placeholder="eg: username" value={hasGeo}/>
                            </div>
                          </Col>
                        </Row>
                        <hr></hr>
                        <Row>
                          <Col>
                            <Form.Label className="h4 mb-4">Numero di Tweet: </Form.Label>
                            <div>
                              <input onChange={setTweetNumberLimit} type="text" placeholder="eg: username" value={tweetNumberLimit}/>
                            </div>
                          </Col>
                          <Col>
                          </Col>
                        </Row>
                        <hr></hr>
                        <Row>
                          <Button
                            onClick={() => setOpen(!open)}
                            aria-controls="example-collapse-text"
                            aria-expanded={open}
                            >
                            Filtra per Mappa
                          </Button>
                          <hr></hr>
                          <Collapse in={open}>
                            <div>
                              {open?<MapFilter key="filterMap" bbox={bbox} setBbox={setBbox} />:<p></p> }
                            </div>
                          </Collapse>
                        </Row>
                        <hr></hr>
                        <center>
                          <Button variant="primary" onClick={() => setShow(false)} className="pl-2">
                              Imposta Filtri
                          </Button>
                        </center>
                      </Container>
                    </Container>
                  </Form.Group>
                </Form>

            </Modal.Body>
          </Modal>
        </Container>
      </Form.Group>
    </Form>
    {loading ? <TabsContainer results={results} distribution={distribution} user={user} />:  <p></p> }
    <hr/>
    {loading ? <ContainerTweet results={results} /> :  <p></p> }
  </Container>
    )
}

export default SearchForm;
