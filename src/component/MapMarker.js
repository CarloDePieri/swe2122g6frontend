import React from 'react';
import {  Marker, Popup } from 'react-leaflet';
import 'leaflet/dist/leaflet.css';
import {Icon} from 'leaflet'
import Pin from '../img/marker.svg'

function returnURLimage(props){
  if(props.media){
    return props.media[0].url;
  }
  else {
    return null;
  }
}

function MapMarker(props)
{
  const tweet = props.tweet;
  const position = [tweet.place.bounding_box.center.lat, tweet.place.bounding_box.center.lon];
  return(
    <Marker
      key = {tweet.id_str}
      position={position}
      icon={
        new Icon({
          iconUrl: Pin,
          iconSize: [30, 30],
        })
    }>
      <Popup>


        <h4> <img style= {{borderRadius: '50%'}} src={tweet.user.img} alt= ""/> {tweet.user.name}</h4>
        <i>{tweet.user.screen_name}</i>
        <p style={{color: 'gray'}}><b></b></p> {tweet.text}
        <hr/>
        <center><img class="d-flex p-2" style={{maxHeight: '200px'}}  src={returnURLimage(tweet)} alt= "" /></center>

      </Popup>
    </Marker>
  )
}

export default MapMarker;
