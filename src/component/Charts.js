import React from "react";
import { Line } from "react-chartjs-2";
import { MDBContainer } from "mdbreact";

let seriesData = [0,2]
let seriesStart=[]


class Charts extends React.Component {

  state = {
    dataLine: {
      labels: seriesStart,
      datasets: [
        {
          label: "Distribution dataset.",
          fill: true,
          lineTension: 0.3,
          backgroundColor: "rgba(225, 204,230, .3)",
          borderColor: "rgb(205, 130, 158)",
          borderCapStyle: "butt",
          borderDash: [],
          borderDashOffset: 0.0,
          borderJoinStyle: "miter",
          pointBorderColor: "rgb(205, 130,158)",
          pointBackgroundColor: "rgb(255, 255, 255)",
          pointBorderWidth: 10,
          pointHoverRadius: 5,
          pointHoverBackgroundColor: "rgb(205, 130, 158)",
          pointHoverBorderColor: "rgba(220, 220, 220,1)",
          pointHoverBorderWidth: 2,
          pointRadius: 1,
          pointHitRadius: 10,
          data: seriesData
        }
      ]
    }
  };

    render() {
      const arr = this.props.distribution

      function initData(props){

        seriesData = [arr.distribution.length]
        let i = 0;
        arr.distribution.map((distribution) => {
            seriesData[i] = distribution.count
            seriesStart[i] = distribution.start.substr(0, 10)
            i = i + 1
          })

       }
       initData(arr)
       this.state.dataLine.labels = seriesStart
       this.state.dataLine.datasets[0].data = seriesData

       return (
         <MDBContainer>

         <h3 className="mt-5">Grafico Distribuzione</h3>
         <Line data={this.state.dataLine} options={{ responsive: true }} />
         </MDBContainer>
        );
      }
}

export default Charts;
