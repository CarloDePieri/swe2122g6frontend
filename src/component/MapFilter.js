import React,{useEffect,useState, useCallback} from 'react';
import { MapContainer, TileLayer, useMap } from 'react-leaflet';
import 'leaflet/dist/leaflet.css';
import MapMarker from './MapMarker'
import Routing from './RoutingMachine'
import './Routing.css'



function MapFilter(props)
{
  const [mappa, setMappa] = useState(null);
/*
  useEffect(() => {
    if(mappa!=null)
    {
      console.log(mappa.getBounds());
    }


  }, [mappa]);*/

  const onMove = useCallback(() => {
    let bbox = mappa.getBounds();
    let bboxString = bbox.getWest() + ',' + bbox.getSouth() + ',' + bbox.getEast() + ',' + bbox.getNorth()
    props.setBbox(bboxString)
  }, [mappa])

  useEffect(() => {
    if(mappa!=null)
    {
      mappa.on('move', onMove)
      return () => {
        mappa.off('move', onMove)
      }
    }

  }, [mappa, onMove])

  const ResizeMap = () => {
    const map = useMap();
    map._onResize();
    return null;
  };
  /*
  const user = props.user
  const results = props.results;
  const positionCenter = [results[0].place.bounding_box.center.lat, results[0].place.bounding_box.center.lon]*/
  return(
    <MapContainer
      style={{ height: "400px" }}
      center={[44.4906, 11.3489]}
      zoom={13} scrollWheelZoom={false}
      className="w-100"
      whenCreated={setMappa}
      >
      <ResizeMap />
      <TileLayer
        attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
        url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
    />
    </MapContainer>
  )
}


export default MapFilter;
