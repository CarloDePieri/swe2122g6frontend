import React from 'react';
import { MapContainer, TileLayer,useMap } from 'react-leaflet';
import 'leaflet/dist/leaflet.css';
import MapMarker from './MapMarker'
import Routing from './RoutingMachine'
import './Routing.css'



function Map(props)
{

  const user = props.user
  const results = props.results;
  const positionCenter = [results[0].place.bounding_box.center.lat, results[0].place.bounding_box.center.lon]

  const ResizeMap = () => {
    const map = useMap();
    map._onResize();
    return null;
  };

  return(
    <MapContainer center={positionCenter} zoom={13} scrollWheelZoom={false} style={{ height: '50vh', width: '100wh' }}>
      <TileLayer
        attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
        url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
    />
  <ResizeMap/>
        {user != "" ? <Routing tweet={results}/> : <p></p>}
  {results.map(tweet => (
      <MapMarker tweet={tweet}/>
    ))}
    </MapContainer>
  )
}


export default Map;
