import L from "leaflet";
import { createControlComponent } from "@react-leaflet/core";
import "leaflet-routing-machine";

const createRoutineMachineLayer = (props) => {

  const tweet = props.tweet

  const data = tweet.map(x => {
    let obj = {
      lat: '',
      lng: ''
    }
    if (x.geo == null) {
      obj.lat = x.place.bounding_box.center.lat;
      obj.lng = x.place.bounding_box.center.lon;
    }
    else
     {
      obj.lat = x.geo.coordinates.lat;
      obj.lng = x.geo.coordinates.lon;
    }
    return obj
  });

  const instance = L.Routing.control({
    addWaypoints: false,
    draggableWaypoints: false,
    createMarker: function() { return null; },
    lineOptions: {
         styles: [{color: 'blue', opacity: 0.45, weight: 4}]
      }
  });

  instance.setWaypoints(data);


  return instance;
};

const RoutingMachine = createControlComponent(createRoutineMachineLayer);

export default RoutingMachine;
