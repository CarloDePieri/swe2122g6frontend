import {Row} from 'react-bootstrap'
import React from 'react'
import TweetCard from "./TweetCard"

function ContainerTweet(props){
  return(
    <div className="ContainerTweet,col">
      <Row md={1} lg={2} xl={3} className="g-4">
      {props.results.map(tweet => (

        <center><TweetCard tweet={tweet}/></center>
      ))
      }
      </Row>
    </div>
  )
}
export default ContainerTweet;
