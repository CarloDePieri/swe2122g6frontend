import 'bootstrap/dist/css/bootstrap.min.css'
import {useState} from 'react'
import { Tabs,Tab } from 'react-bootstrap'
import Map from './Map'
import Charts from './Charts'
import PieChart from './ChartsPage'
import { MapContainer, TileLayer } from 'react-leaflet';
import WordCloud from './WordCloud'


function TabsContainer(props) {
  const [key, setKey] = useState('map');
  const arr = props.results
  const user = props.user
  //const tweetNumberLimit = props.tweetNumberLimit
  return (

    <Tabs
      id="controlled-tab-example"
      activeKey={key}
      onSelect={(k) => setKey(k)}
      className="mb-3"
    >
      <Tab eventKey="map" title="Mappa">
      {arr.filter(element => element.geo !== null || element.place !== null).length ? <Map user = {user} results={arr.filter(element => element.geo !== null || element.place !== null)}/> :
      <MapContainer center={[44.4906, 11.3489]} zoom={13} scrollWheelZoom={false} className="w-100">
        <TileLayer
          attribution='&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
          url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
          />
      </MapContainer>
    }
      </Tab>

      <Tab eventKey="distribution" title="Distribuzione">
       {/*<Table distribution={props.distribution}/>*/}
       <Charts distribution={props.distribution}/>
       <PieChart results={props.results}/>
      </Tab>

      <Tab eventKey="wordcloud" title="Wordcloud">
        {arr.length ? <WordCloud tweets={props.results} eventKey={key} /> :  <p></p> }
      </Tab>
    </Tabs>

  );
}

export default TabsContainer;
