import React from 'react';
import SearchForm from './SearchForm';
import {screen, fireEvent, render} from '@testing-library/react';
 /*
beforeEach(() => {
  fetch.resetMocks();
});

it ("finds exchange", async () => {
  fetch.mockResponseOnce(JSON.stringify({
  "results": [
    {
      "id_str": "1462869544226197514",
      "text": "@fabius10scudi @GarauPina @Gui_Pap @StefanoBerto83 Io soni stato bloccato dal compare.\nCredono che due leccate di culo farà guadagnare loro un posto al sole, ma sono solo pollame in batteria.",
      "screen_name": "@ZombieBuster5",
      "geo": null,
      "place": null
    }
  ]
}));

  const wrapper = shallow(<InputForm/>);
     wrapper.find('#search-tweet-button').simulate('click');
     expect(wrapper.find('#search-results').children()).to.have.lengthOf(1);

});
*/

it ("Calls the onSubmitFn when the button is clicked", () => {
  const onSubmitMock = jest.fn();
  render(<SearchForm onSubmit={onSubmitMock} />);
  const button = screen.getByText(/Cerca/i, {selector: 'button'});
  fireEvent.click(button);
  expect(onSubmitMock.mock.calls.length).toBe(1);
});

it (" When filter button is selected the modal component is shown", () => {
  const onSubmitMock = jest.fn();
  render(<SearchForm onSubmit={onSubmitMock} />);
  const button = screen.getByText(/Filtri/i, {selector: 'button'});
  fireEvent.click(button);

  expect(screen.getByRole('dialog')).toHaveClass("show");
});
