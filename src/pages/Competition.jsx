import 'bootstrap/dist/css/bootstrap.min.css'
import '../index.css'
import '../App.css';
import {Col, Nav, Navbar, Row,Tabs,Tab} from "react-bootstrap"
import {Container} from "@chakra-ui/react"
import Form from 'react-bootstrap/Form'
import Button from 'react-bootstrap/Button'
import React, {useState} from 'react'

function Competition(){
  const [key, setKey] = useState('competition');
  const [isOkComp, setIsOkComp] = useState(false);
  const [isOkTrivia, setIsOkTrivia] = useState(false);

  function onSubmit(){

    try {

    } catch (e) {

    }

  }

  return(
    <div className="Trivia">
      <Container className="appHeader">
        <Row>
          <Col></Col>
          <Col xs={8}>
            <Row className="flex-grow-1 align-items-center" style={{minHeight: "20vh"}}>
              <Col></Col>
              <Col xs={8} className="text-center">
                <Container style={{border: "solid white 4px", borderRadius: "20px"}}>
                  <h1><i className="fa fa-twitter"></i> Awesome Twitter Client</h1>
                </Container>
              </Col>
              <Col className="text-end"></Col>
            </Row>
          </Col>
          <Col></Col>
        </Row>
      </Container>
      <Navbar sticky="top" expand="lg" className="appHeader">
        <Container>
          <Navbar.Toggle aria-controls="basic-navbar-nav" />
          <Navbar.Collapse id="basic-navbar-nav">
            <Nav className="me-auto">
              <Nav.Link href="http://localhost:3000" style={{color: "white", font:"Roboto"}} className="appHeaderLink h3" >Home</Nav.Link>
              <Nav.Link href="http://localhost:3000/trivia" style={{color: "white", font:"Roboto"}} className="appHeaderLink h3" >Trivia</Nav.Link>
              <Nav.Link href="http://localhost:3000/competition" style={{color: "white", font:"Roboto"}} className="appHeaderLink h3" >Competition</Nav.Link>
            </Nav>
          </Navbar.Collapse>
        </Container>
      </Navbar>
      <Container>
        <Row>
          <Col></Col>
          <Col xs={10} className="appBody">
                <Form style={{paddingTop: "5rem"}}>
                  <Form.Group controlId="formGridEmail">
                    <Form.Label className="h3 mb-4" style={{font:"Roboto"}}>Inserisci il link alla competizione</Form.Label>
                    <Container className="mb-4 d-flex">
                      <Container className="flex-grow-1">
                        <div>
                          <Form.Control type="text" placeholder="eg: #newyork" />
                        </div>
                      </Container>
                      <Container className="px-2">
                        <Button variant="primary" type="submit" className="pl-2">
                          Cerca
                        </Button>
                      </Container>
                    </Container>
                  </Form.Group>
                </Form>
                {isOkComp?<p>Trovata</p>
                :
                <p>Nessuna competizione trovata</p>

                }
          </Col>
          <Col></Col>
        </Row>
      </Container>

    </div>
  )
}

export default Competition;
