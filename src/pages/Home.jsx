import React from 'react';
import SearchForm from '../component/SearchForm';
import 'bootstrap/dist/css/bootstrap.min.css'
import '../index.css'
import '../App.css';
import {Col, Nav, Navbar, Row} from "react-bootstrap"
import {Container} from "@chakra-ui/react"
import Form from 'react-bootstrap/Form'

function Home(){
  return(
    <div className="App">
      <Container className="appHeader">
        <Row>
          <Col></Col>
          <Col xs={8}>
            <Row className="flex-grow-1 align-items-center" style={{minHeight: "20vh"}}>
              <Col></Col>
              <Col xs={8} className="text-center">
                <Container style={{border: "solid white 4px", borderRadius: "20px"}}>
                  <h1><i className="fa fa-twitter"></i> Awesome Twitter Client</h1>
                </Container>
              </Col>
              <Col className="text-end"></Col>
            </Row>
          </Col>
          <Col></Col>
        </Row>
      </Container>
      <Navbar sticky="top" expand="lg" className="appHeader">
        <Container>
          <Navbar.Toggle aria-controls="basic-navbar-nav" />
          <Navbar.Collapse id="basic-navbar-nav">
            <Nav className="me-auto">
              <Nav.Link href="http://localhost:3000" style={{color: "white", font:"Roboto"}} className="appHeaderLink h3" >Home</Nav.Link>
              <Nav.Link href="http://localhost:3000/trivia" style={{color: "white", font:"Roboto"}} className="appHeaderLink h3" >Trivia</Nav.Link>
              <Nav.Link href="http://localhost:3000/competition" style={{color: "white", font:"Roboto"}} className="appHeaderLink h3" >Competition</Nav.Link>
            </Nav>
          </Navbar.Collapse>
        </Container>
      </Navbar>

      <Container>
        <Row>
          <Col></Col>
          <Col xs={8} className="appBody"><SearchForm/></Col>
          <Col></Col>
        </Row>
      </Container>

    </div>
  )
}

export default Home;
