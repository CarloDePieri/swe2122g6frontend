import Home from "./pages/Home";
import Trivia from "./pages/Trivia";
import Competition from "./pages/Competition"
import { Routes, Route } from "react-router-dom";


function App() {
  return (
    <Routes>
      <Route path="/" element={<Home />} />
      <Route path="/trivia" element={<Trivia />} />
      <Route path="/competition" element={<Competition />} />
    </Routes>
  );
}
export default App;
