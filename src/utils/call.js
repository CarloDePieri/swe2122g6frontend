const axios= require('axios');

const serverApi = "http://localhost:8000/api"

function makeStringQuery(query,user,place,limit){
  let stringQuery = `${serverApi}/search?`
  let a=[];
  if(query!=="")
  {
    stringQuery=stringQuery+'q='+query
    a.push('q='+query);
  }
  if(user!=="")
  {
    stringQuery=stringQuery+'&u='+user
    a.push('u='+user);
  }
  if(place!=="")
  {
    stringQuery=stringQuery+'&p='+place
    a.push('p='+place);
  }
  stringQuery=stringQuery+'&limit='+limit
  a.push('limit='+limit);
  console.log(a.join('&'));
  console.log(stringQuery);
  //return stringQuery
  return stringQuery+a.join('&');


}

export async function getTweets(query="",user="",place="",limit=10){
  let results;
  results = await axios.get(makeStringQuery(query,user,place,limit));
  console.log(results.data);
  return results.data

}

export async function getSentiment(tweets)
{
  let results;
   try {
      results = await axios.post('http://localhost:8000/api/sentiment',tweets)
   }
   catch (err) {
     console.log(err)
   }
  return results.data
}
