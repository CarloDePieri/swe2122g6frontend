const TextCleaner = require('text-cleaner');

function cleanText(text) {
  // Regex that match puctuation characters, the special characters … used by twitter
  const excludedCharRegex = /(\.|,|-|:|\.{3}|…)/g

  return TextCleaner(text).toLowerCase().stripHtml().condense().removeStopWords()
                            .trim().replace(excludedCharRegex, '');
}

function calculateWordsFrequency(words){
  const wordCounts = {};
  for(let i = 0; i < words.length; i++){
      wordCounts[words[i]] = (wordCounts[words[i]] || 0) + 1
  }
  return wordCounts
}

function sortDictionaryByValue(dict, limit) {
  // Create items array
  let items = Object.keys(dict).map(function(key) {
    return [key, dict[key]];
  });
  // Sort the array based on the second element
  items.sort(function(first, second) {
    return second[1] - first[1];
  });
  items = items.splice(0, limit);
  const sortedObj = []
  items.forEach(elem => {
      const obj = {
        text: elem[0],
        value: elem[1]
      }
      sortedObj.push(obj);
  })
  return sortedObj;
}



export function getWordCount(tweets){
  const wordCount = 50
  const minWordLength = 3
  const maxWordLength = 50
  const texts = tweets.map(tweet => tweet.text)
  const arrayTweet = texts.map(tweet => tweet.split(/\s/))
  const words = arrayTweet.flat(1)
  const cleanedWords = words.map(x => cleanText(x)).filter(x => (x.length > minWordLength) && (x.length < maxWordLength))
  const frequency = calculateWordsFrequency(cleanedWords)
  return sortDictionaryByValue(frequency, wordCount)


}
