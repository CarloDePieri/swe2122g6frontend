Feature: TG-35

  As an User
  I want to look up geolocalized tweets from a Twitter user
  So that I can keep up with him in case of an emergency

  Scenario: Viewing a Twitter User's geolocalized tweets
    When the User perform a 'Twitter User's position' search
    Then the Twitter User's tweets with geolocation data are shown

  Scenario: Viewing tweets from a second search
    Given the User already performed a 'Twitter User position' search
    When the User performs another search
    Then older tweets disappear
    And the new ones are shown

  Scenario: Searching returns no results
    When the User performs a query that returns no result
    Then an alert is shown
    And the search interface shows 'no results found'
