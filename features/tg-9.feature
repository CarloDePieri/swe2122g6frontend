Feature: TG-9

  As a User
  I want to search tweets by hashtag
  So that I may inspect them in a simple ui

  Scenario: Viewing tweets from a search
    Given the User has the search page open
    When the User performs a search
    Then the relevant tweets, with text and username, are shown

  Scenario: Viewing tweets from a second search
    Given the User already pressed the button once
    When the User performs another search
    Then older tweets disappear
    And the new ones are shown

  Scenario: Searching returns no results
    When the User performs a query that returns no result
    Then an alert is shown
    And the search interface shows 'no results found'
