Feature: TG-18

  As an User
  I want to see the time distribution of tweets after a search
  So that I can see how interest about a topic changes with time

  Scenario: Viewing distribution table
    Given the User has the search page open
    When the User performs the search
    Then the distribution data appear in a table
 
  Scenario: Viewing table from a second search
    Given the User already searched once
    When the User performs another search
    Then older results disappear
    And the new ones are shown in the table
